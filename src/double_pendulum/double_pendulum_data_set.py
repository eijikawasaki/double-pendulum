import numpy as np
import pandas as pd
from torch.utils.data import DataLoader, Dataset, Subset

from .double_pendulum import DoublePendulum


class DoublePendulumDataset(Dataset):
    target_columns = ('x1', 'x2', 'y1', 'y2')

    def __init__(self, max_time=1000, dt=0.1, min_shift=20, max_shift=25, sample_step=1, float_divider=1.,
                 mass_1=1., mass_2=1., initial_condition=(.5, 0., -2.5, 0.)):
        """
        In order to create the features, the positions of the double pendulum is shifted by a desired number of periods.

        Args:
            max_time: end time of the simulation
            dt: time step
            min_shift: start of the shift
            max_shift: end of the shift (min_shift must be smaller than max_shift)
            sample_step: trims data by recording the data at each sample_step instead of each data point
            float_divider: data is divided by this number. Default value is set to 1 such that the maximum of the data
            is equal to 1.
            mass_1:
            mass_2:
        """
        df = self._create_raw_df(tmax=max_time, dt=dt, step=sample_step, mass_1=mass_1, mass_2=mass_2,
                                 initial_condition=initial_condition)
        df = df.div(float_divider)
        if min_shift is not None:
            assert min_shift < max_shift
            df = DoublePendulumDataset._make_lag(df, min_shift, max_shift)
            df = DoublePendulumDataset._remove_first_rows(df, max_shift + 1)
        self._df = df
        self.columns = df.columns.to_numpy()

    def get_df(self):
        return self._df

    def get_csv_data(self):
        return self._df.to_csv(index=False)

    def get_train_test_data_loaders(self, batch_size=None, is_random=False, validation_split=.7):
        """
        output size is 4: x1, x2, y1, y2

        Args:
            batch_size: batch size for the pytorch dataLoaders. If no batch_size is given, the full data is loaded.
            is_random:
            validation_split:

        Returns:
            train_loader: train data loader
            validation_loader: test  data loader
        """
        dataset_size = len(self)
        if batch_size is None:
            batch_size = dataset_size

        train_indices, val_indices = DoublePendulumDataset._create_train_test_indices(validation_split, dataset_size)
        train_loader, validation_loader = self._create_sequential_data_loaders(batch_size, train_indices, val_indices,
                                                                               is_shuffled=is_random)
        return train_loader, validation_loader

    @staticmethod
    def _create_train_test_indices(validation_split, dataset_size):
        """
        https://pytorch.org/tutorials/recipes/recipes/custom_dataset_transforms_loader.html
        """
        indices = list(range(dataset_size))
        split = int((1. - validation_split) * dataset_size)
        train_indices, val_indices = indices[:split], indices[split:]
        return train_indices, val_indices

    def _create_sequential_data_loaders(self, batch_size, train_indices, val_indices, is_shuffled):
        train_loader = DataLoader(Subset(self, train_indices), batch_size=batch_size, shuffle=is_shuffled)
        validation_loader = DataLoader(Subset(self, val_indices), batch_size=batch_size, shuffle=is_shuffled)
        return train_loader, validation_loader

    def __len__(self):
        return len(self._df)

    def __getitem__(self, idx):
        feature = self._df.iloc[idx, ~self._df.columns.isin(self.target_columns)].to_numpy(dtype=np.float32)
        labels = self._df.iloc[idx, self._df.columns.isin(self.target_columns)].to_numpy(dtype=np.float32)
        return feature, labels

    @staticmethod
    def _create_raw_df(tmax, dt, step, mass_1, mass_2, initial_condition):
        x1, x2, y1, y2 = DoublePendulum(mass_1=mass_1, mass_2=mass_2,
                                        initial_condition=initial_condition).generate_data(max_time=tmax, dt=dt)
        x1, x2, y1, y2 = DoublePendulumDataset._trim_data(step, x1, x2, y1, y2)
        df = pd.DataFrame({'x1': x1, 'x2': x2, 'y1': y1, 'y2': y2})
        return df

    @staticmethod
    def _trim_data(step, x1, x2, y1, y2):
        return x1[::step], x2[::step], y1[::step], y2[::step]

    @classmethod
    def _make_lag(cls, df, min_shift, max_shift):
        for column in cls.target_columns:
            for shift in range(min_shift, max_shift):
                df[f'lag({column},{shift})'] = df[f'{column}'].shift(shift)
                df = df.copy()
        return df

    @staticmethod
    def _remove_first_rows(df, rows_to_be_removed):
        df = df.iloc[rows_to_be_removed:, :]
        return df
